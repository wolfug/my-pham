<!DOCTYPE html>
<html>
<head>
    <title>Cosmetic</title>
    <link href="{{asset('web/css/bootstrap.css')}}" rel="stylesheet" type="text/css" media="all" />
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="{{ asset('bower_components/jquery/dist/jquery.min.js') }}"></script>
    <!-- Custom Theme files -->
    <!--theme-style-->
    <link href="{{asset('web/css/style.css')}}" rel="stylesheet" type="text/css" media="all" />
    <link href="{{ asset('bower_components/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" media="all" />
    <!--//theme-style-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="Cosmetic" />
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    <!-- start menu -->
    <script src="{{asset('web/js/simpleCart.min.js')}}"> </script>
    <!-- start menu -->
    <link href="{{asset('web/css/memenu.css')}}" rel="stylesheet" type="text/css" media="all" />
    <script type="text/javascript" src="{{asset('web/js/memenu.js')}}"></script>
    <script>$(document).ready(function(){$(".memenu").memenu();});</script>
    <!-- /start menu -->
</head>
<body>
<!--header-->
<div class="top_bg">
    <div class="container">
        <div class="header_top-sec">
            <div class="top_right">
                <ul>
                    <li><a href="{{route('frontend_contact')}}">Liên hệ</a></li>
                </ul>
            </div>
            <div class="top_left">
                <ul>
                    @auth
                        <li class="top_link">Xin chào : {{auth()->user()->name}}</li>
                        <li class="top_link"><a href="{{route('logout')}}">Đăng xuất</a></li>
                        @if(auth()->user()->type == \App\User::ADMIN)
                            <li class="top_link"><a href="{{route('index')}}">Trang quản trị</a></li>
                        @endif
                        @else
                            <li class="top_link"><a href="{{route('login')}}">Đăng nhập</a></li>
                        @endauth

                </ul>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>
</div>
<div class="header-top">
    <div class="header-bottom">
        <div class="container">
            <div class="logo">
                <a href="{{route('frontend.index')}}"><h1>Cosmetic Store</h1></a>
            </div>
            <!---->

            <div class="top-nav">
                <ul class="memenu skyblue">
                    <li class="{{ Request::is('/') ? 'active' : '' }}"><a href="{{route('frontend.index')}}">Trang chủ</a></li>
                    <li class="grid {{ Request::is('product') ? 'active' : '' }}"><a href="{{route('frontend.product')}}">Sản phẩm</a></li>
                </ul>
                <div class="clearfix"> </div>
            </div>
            <!---->
            <div class="cart box_1">
                <a href="{{route('all_cart.product')}}">
                    <h3> <div class="total">{{\Cart::count()}}</div>
                        <span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span></h3>
                </a>
                <div class="clearfix"> </div>
            </div>
            <div class="clearfix"> </div>
            <!---->
        </div>
        <div class="clearfix"> </div>
    </div>
</div>
<div class="flash-message text-center">
    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
        @if(session('alert-' . $msg))
            <div class="alert alert-{{ $msg }}">
                {{ session('alert-' . $msg) }}
            </div>
        @endif
    @endforeach
</div>
@yield('content')
<!---->
<div class="shoping">
    <div class="container">
        <div class="shpng-grids">
            <div class="col-md-4 shpng-grid">
                <h3>Miễn phí vận chuyển</h3>
                <p>Đơn hàng trên 1.000.000 vnđ</p>
            </div>
            <div class="col-md-4 shpng-grid">
                <h3>Chính sách đổi trả</h3>
                <p>Đổi trả miễn phí sau 10 ngày</p>
            </div>
            <div class="col-md-4 shpng-grid">
                <h3>COD</h3>
                <p>Thanh toán khi giao hàng</p>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>

<div class="footer">
    <div class="container">
        <div class="ftr-grids">
            <div class="col-md-3 ftr-grid">
                <h4>About Us</h4>
                <ul>
                    <li><a href="#">Who We Are</a></li>
                    <li><a href="contact.html">Contact Us</a></li>
                    <li><a href="#">Our Sites</a></li>
                    <li><a href="#">In The News</a></li>
                    <li><a href="#">Team</a></li>
                    <li><a href="#">Careers</a></li>
                </ul>
            </div>
            <div class="col-md-3 ftr-grid">
                <h4>Customer service</h4>
                <ul>
                    <li><a href="#">FAQ</a></li>
                    <li><a href="#">Shipping</a></li>
                    <li><a href="#">Cancellation</a></li>
                    <li><a href="#">Returns</a></li>
                    <li><a href="#">Bulk Orders</a></li>
                    <li><a href="#">Buying Guides</a></li>
                </ul>
            </div>
            <div class="col-md-3 ftr-grid">
                <h4>Your account</h4>
                <ul>
                    <li><a href="account.html">Your Account</a></li>
                    <li><a href="#">Personal Information</a></li>
                    <li><a href="#">Addresses</a></li>
                    <li><a href="#">Discount</a></li>
                    <li><a href="#">Track your order</a></li>
                </ul>
            </div>
            <div class="col-md-3 ftr-grid">
                <h4>Categories</h4>
                <ul>
                    <li><a href="#">> Cosmetic</a></li>
                    <li><a href="#">> Jewellerys</a></li>
                    <li><a href="#">> Shoes</a></li>
                    <li><a href="#">> Flowers</a></li>
                    <li><a href="#">> Cakes</a></li>
                    <li><a href="#">...More</a></li>
                </ul>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<!---->
<div class="copywrite">
    <div class="container">
        <p>Copyright © 2015 Cosmetic Store. All Rights Reserved | Design by <a href="http://w3layouts.com">W3layouts</a></p>
    </div>
</div>

<!-- //footer -->
<!-- cart-js -->
<script src="{{ asset('bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('bower_components/select2/dist/js/select2.min.js') }}"></script>
<script src="{{ asset('bower_components/trumbowyg/dist/trumbowyg.min.js') }}"></script>
<script src="{{ asset('js/admin/common.js') }}"></script>
@stack('scripts')
<!--Start of Tawk.to Script-->
<script type="text/javascript">
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
        var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
        s1.async=true;
        s1.src='https://embed.tawk.to/59ef3f0a4854b82732ff7637/default';
        s1.charset='UTF-8';
        s1.setAttribute('crossorigin','*');
        s0.parentNode.insertBefore(s1,s0);
    })();
</script>
<!--End of Tawk.to Script-->
@stack('scripts')
</body>
</html>