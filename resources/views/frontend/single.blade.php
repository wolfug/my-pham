@extends('frontend.layouts.app')
@section('title', 'index')
@section('content')
<div class="container">
    <ol class="breadcrumb">
        <li><a href="{{route('frontend.index')}}">Trang chủ</a></li>
        <li class="active">Chi tiết sản phẩm</li>
    </ol>
    <h2>Chi tiết sản phẩm</h2>
        <!-- single -->
    <div class="single">
        <div class="container">
            <div class="col-md-4 single-left">
                <div class="flexslider">
                        <img src="{{route('product.image', $product->id)}}" data-imagezoom="true" class="img-responsive" alt="{{$product->name}}">
                </div>
            </div>
            <div class="col-md-8 single-right">
                <h3>{{$product->name}}</h3>
                <div class="description">
                    <h5><i>{{trans('messages.desc_product_lable')}}</i></h5>
                    <p>{!! $product->desc!!}</p>
                </div>
                <div class="simpleCart_shelfItem">
                    <p><i class="item_price">{{number_format($product->price, 0, ',', '.')}} vnđ</i></p>
                    <div>
                        <a href="{{route('cart.product', $product->id)}}" class="w3ls-cart item_add items">{{trans('messages.add_to_cart')}}</a>
                    </div>
                </div>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>
    <!-- //single -->
</div>
@endsection